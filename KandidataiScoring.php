<?php

namespace kadidatai;

use DateTime;
use DateTimeZone;

class KandidataiScoring
{

    protected $data = array();
    protected $scores = array();
    protected $scoreValues = array(
        'oldAge' => -2,
        'education' => -3,
        'masters' => 2,
        'doctor' => 6,
        'sovietEducation' => -3,
        'notActualEducation' => -1,
        'noEnglish' => -3,
        'otherLanguages' => 2,
        'convict' => -3,
        'timesElected' => -1,
        'noIncome' => -2,
        'noTax' => -2,
    );

    public function setData($data)
    {
        $this->data = $data;
    }

    public function setScoreValues($scoreValues)
    {
        $this->scoreValues = $scoreValues;
    }

    public function getScores()
    {
        if (empty($this->data)) {
            return array();
        }
        $limit = 10000;
        $count = 0;
        foreach ($this->data as $candidate) {
            if ($count > $limit) {
                break;
            }
            $this->scores[] = $this->score($candidate);
            $count++;
        }
        $this->sortScores();
        return $this->scores;
    }

    protected function sortScores()
    {
        $n = count($this->scores);
        for ($i = 0; $i < $n; $i++) {
            for ($i2 = 0; $i2 < $n - 1; $i2++) {
                if ($this->scores[$i2]['score'] < $this->scores[$i2 + 1]['score']) {
                    $temp = $this->scores[$i2 + 1];
                    $this->scores[$i2 + 1] = $this->scores[$i2];
                    $this->scores[$i2] = $temp;
                    unset($temp);
                }
            }
        }
    }

    protected function score($candidate)
    {
        $score = $this->buildEmptyScoreTable();
        if (empty($candidate)) {
            return $score;
        }
        foreach (array_keys($score) as $fieldName) {
            if ($fieldName == 'score') {
                continue;
            }

            $functionName = 'get' . ucfirst($fieldName);
            if (method_exists($this, $functionName)) {
                $score[$fieldName] = $this->$functionName($candidate);
            } else {
                echo $functionName . ' still not done' . PHP_EOL;
            }
        }
        $score['score'] = $this->countScore($score);
        return $score;
    }

    protected function countScore($scoreData)
    {
        $score = 0;
        foreach ($this->scoreValues as $key => $var) {
            if (!empty($scoreData[$key])) {
                $score += (int) $scoreData[$key] * $var;
            }
        }
        return $score;
    }

    protected function getUrl($candidate)
    {
        if (empty($candidate['id'])) {
            return '';
        }
        return 'http://www.vrk.lt/2016-seimo/kandidatai?srcUrl=kandidatai/lrsKandidatasAnketaHtml%3FrkndId%3D' . $candidate['id'] . '%26rnkId%3D426';
    }

    protected function getName($candidate)
    {
        if (empty($candidate['name'])) {
            return '';
        }
        return $candidate['name'];
    }

    protected function getPartyList($candidate)
    {
        if (empty($candidate['party_list'])) {
            return '';
        }
        return $candidate['party_list'];
    }

    protected function getPartyNumber($candidate)
    {
        if (empty($candidate['party_num'])) {
            return 0;
        }
        return (int) $candidate['party_num'];
    }

    protected function getAge($candidate)
    {
        if (empty($candidate['birth'])) {
            return 0;
        }
        $tz = new DateTimeZone('Europe/Vilnius');
        $age = (new DateTime($candidate['birth'], $tz))
                        ->diff(new DateTime('now', $tz))->y;
        return $age;
    }

    protected function getOldAge($candidate)
    {
        if (empty($candidate['birth'])) {
            return 0;
        }
        $tz = new DateTimeZone('Europe/Vilnius');
        $age = (new DateTime($candidate['birth'], $tz))
                        ->diff(new DateTime('now', $tz))->y;
        return $age >= 65 ? 1 : 0;
    }

    protected function getNoEducation($candidate)
    {
        if (empty($candidate['education'])) {
            return 1;
        }
        if (!is_array($candidate['education'])) {
            return 1;
        }
        foreach ($candidate['education'] as $edu) {
            if (strpos($edu['level'], 'Aukštasis') !== false) {
                return 0;
            }
        }
        if ($this->getMasters($candidate) || $this->getDoctor($candidate)) {
            return 0;
        }
        return 1;
    }

    protected function getMasters($candidate)
    {
        if (empty($candidate['science_name'])) {
            return 0;
        }
        if (strpos(strtolower($candidate['science_name']), 'magistr') !== false) {
            return 1;
        }
        return 0;
    }

    protected function getDoctor($candidate)
    {
        $prefixes = array('daktar', 'docent', 'prof', 'gydytoj', 'dr');
        if (empty($candidate['science_name'])) {
            return 0;
        }
        foreach ($prefixes as $prefix) {
            if (strpos(strtolower($candidate['science_name']), $prefix) !== false) {
                return 1;
            }
        }
        return 0;
    }

    protected function getSovietEducation($candidate)
    {
        if (empty($candidate['education'])) {
            return 0;
        }
        if (!is_array($candidate['education'])) {
            return 0;
        }
        $max = null;
        foreach ($candidate['education'] as $edu) {
            if (!isset($max) || $edu['year'] > $max) {
                $max = $edu['year'];
            }
        }
        return $max > 0 && $max < 1990 ? 1 : 0;
    }

    protected function getNotActualEducation($candidate)
    {
        if (empty($candidate['education'])) {
            return 1;
        }
        if (!is_array($candidate['education'])) {
            return 1;
        }

        $actualSpec = array('vadyb', 'teis', 'politik', 'istor');
        foreach ($candidate['education'] as $edu) {
            if (empty($edu['spec'])) {
                continue;
            }
            foreach ($actualSpec as $spec) {
                if (strpos($edu['spec'], $spec) !== false) {
                    return 0;
                }
            }
        }

        return 1;
    }

    protected function getNoEnglish($candidate)
    {
        if (empty($candidate['langs'])) {
            return 1;
        }
        return in_array("Anglų", $candidate['langs']) ? 0 : 1;
    }

    protected function getOtherLanguages($candidate)
    {
        if (empty($candidate['langs'])) {
            return 0;
        }
        $lans = array_diff($candidate['langs'], array("Anglų", "Rusų"));
        return count($lans);
    }

    protected function getConvict($candidate)
    {
        if (empty($candidate['convicted'])) {
            return 0;
        } else {
            return 1;
        }
    }

    protected function getTimesElected($candidate)
    {
        if (empty($candidate['political_career'])) {
            return 0;
        }
        if (!is_array($candidate['political_career'])) {
            return 0;
        }
        $count = 0;
        foreach ($candidate['political_career'] as $row) {
            if (strpos($row['name'], 'Seimo narys') !== false) {
                $count++;
            }
        }
        return $count;
    }

    protected function getNoIncome($candidate)
    {
        if (empty($candidate['income'])) {
            return 1;
        }

        return ($this->fixAmounts($candidate['income']) < 100 ? 1 : 0);
    }

    protected function getNoTax($candidate)
    {
        if (empty($candidate['taxes'])) {
            return 1;
        }
        $taxes = $this->fixAmounts($candidate['income']);
        $income = !empty($candidate['income']) ? $this->fixAmounts($candidate['income']) : 0;
        if (!empty($income)) {
            return ($taxes / $income) < 0.01 ? 1 : 0;
        }
        return 0;
    }

    protected function fixAmounts($amount)
    {
        $amountS = str_replace(',', '.', substr($amount, 0, -4));
        return floatval(trim($amountS));
    }

    protected function buildEmptyScoreTable()
    {
        $sTable = array(
            'score' => 0,
            'name' => '',
            'partyList' => '',
            'partyNumber' => 0,
            'age' => 0,
            'oldAge' => 0,
            'noEducation' => 0,
            'masters' => 0,
            'doctor' => 0,
            'sovietEducation' => 0,
            'notActualEducation' => 0,
            'noEnglish' => 0,
            'otherLanguages' => 0,
            'convict' => 0,
            'timesElected' => 0,
            'noIncome' => 0,
            'noTax' => 0,
            'url' => '',
        );
        return $sTable;
    }
}
