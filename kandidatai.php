<?php

namespace kadidatai;

use kadidatai\KandidataiScoring;

include_once('KandidataiScoring.php');

$scoreValues = array(
    'oldAge' => -2,
    'education' => -3,
    'masters' => 2,
    'doctor' => 6,
    'sovietEducation' => -3,
    'notActualEducation' => -1,
    'noEnglish' => -3,
    'otherLanguages' => 2,
    'convict' => -3,
    'timesElected' => -1,
    'noIncome' => -2,
    'noTax' => -2,
);

$fileData = file_get_contents('kandidatai.json');
$data = json_decode($fileData, true);

$scoring = new KandidataiScoring();
$scoring->setData($data);
$scoring->setScoreValues($scoreValues);
$scores = $scoring->getScores();


$fo = fopen('kandidatai.csv', 'w');

$titles = array(
    'score' => 'Taškai',
    'name' => 'Vardas',
    'partyList' => 'Partija',
    'partyNumber' => 'Numeris',
    'age' => 'Amžius',
    'oldAge' => '>=65 metai',
    'noEducation' => 'Neturi aukštojo',
    'masters' => 'Turi magistrą',
    'doctor' => 'Turi daktaro laipsnį',
    'sovietEducation' => 'Tarybinis išsilavinimas',
    'notActualEducation' => 'Neaktualus išsilavinimas',
    'noEnglish' => 'Nemoka anglų kalbos',
    'otherLanguages' => 'Moka kitų kalbų (be anglų ir rusų)',
    'convict' => 'Teistas',
    'timesElected' => 'Išrinktas į seimą',
    'noIncome' => 'Neturi pajamų',
    'noTax' => 'Nemoka mokesčių',
    'id' => 'Kandidato ID',
    'url' => 'Nuoroda į VRK',
);

$titleRow = array();
foreach (array_keys($scores[0]) as $key) {
    $titleRow[] = !empty($titles[$key]) ? $titles[$key] : $key;
}
fputs($fo, '"' . join("\"\t\"", $titleRow) . '"' . PHP_EOL);
foreach ($scores as $row) {
    fputs($fo, '"' . join("\"\t\"", $row) . '"' . PHP_EOL);
}
fclose($fo);
